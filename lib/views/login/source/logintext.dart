part of datasource;

class LoginText {
  static String title = 'Login';

  static String formUsername = 'Nama Pengguna';
  static String formPassword = 'Kata Sandi';

  static String buttonLogin = 'Masuk';

  static String footerCopyright = 'CopyRight 2021 Admin Template';
}