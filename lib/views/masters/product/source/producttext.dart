part of datasource;

class ProductText{
  static String title = 'Produk';
  static String subTitle= 'Data Master Produk';

  static String formType= 'Tipe';
  static String formCode = 'Kode';

  static String tableTypeId = 'typeid';
  static String tableCode = 'productcd';
  static String tableName = 'productnm';

}